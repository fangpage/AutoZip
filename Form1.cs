﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FangPage.Common;

namespace AutoZip
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Config config = new Config();

        System.Timers.Timer timer = new System.Timers.Timer();//定时器

        bool is_start = false;//是否已启动

        int hour = 0;

        int minute = 0;

        object lock_timer = new object();//定时锁

        private void Form1_Load(object sender, EventArgs e)
        {
            config = FPXml.LoadModel<Config>(FPFile.GetMapPath("/config.xml"));

            txtBackup.Text = config.backup;
            txtSave.Text = config.saveas;

            cmbHour.Text = config.hour;
            cmbMini.Text = config.minute;

            hour = FPUtils.StrToInt(config.hour);

            minute = FPUtils.StrToInt(config.minute);

            if (config.autorun == 1)
            {
                btnStart_Click(null, null);
            }

            //启动计时器
            timer.Interval = 1000;
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(TimerElapsed);
        }

        private void TimerElapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            lock (lock_timer)
            {
                if (!is_start)
                {
                    return;
                }

                if (!Directory.Exists(config.backup))
                {
                    return;
                }

                int m_hour = DateTime.Now.Hour;//时

                int m_second = DateTime.Now.Minute * 60 + DateTime.Now.Second;//秒

                if (m_hour == hour && m_second == minute * 60)
                {
                    FPZip fpzip = new FPZip();

                    fpzip.AddDir(config.backup);

                    string zip_filename = Path.GetFileName(config.backup) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");

                    if (!Directory.Exists(config.saveas))
                    {
                        FPFile.CreateDir(config.saveas);
                    }

                    fpzip.ZipSave(config.saveas + "\\" + zip_filename + ".zip");

                    fpzip.Close();
                    fpzip.Dispose();
                }
            }
        }

        private void btnBackup_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择备份路径";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txtBackup.Text = dialog.SelectedPath;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择存放路径";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                txtSave.Text = dialog.SelectedPath;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {

            if (!Directory.Exists(txtBackup.Text))
            {
                MessageBox.Show("备份文件夹不存在。");
                btnStop_Click(null, null);
                return;
            }

            if (string.IsNullOrEmpty(cmbHour.Text) || string.IsNullOrEmpty(cmbMini.Text))
            {
                MessageBox.Show("备份时间格式不正确。");
                btnStop_Click(null, null);
                return;
            }

            is_start = true;

            config.backup = txtBackup.Text;
            config.saveas = txtSave.Text;
            config.hour = cmbHour.Text;
            config.minute = cmbMini.Text;
            config.autorun = 1;

            hour = FPUtils.StrToInt(config.hour);

            minute = FPUtils.StrToInt(config.minute);

            FPXml.SaveXml<Config>(config, FPFile.GetMapPath("/config.xml"));

            btnStart.Enabled = false;
            btnStop.Enabled = true;

            btnBackup.Enabled = false;
            btnSave.Enabled = false;
            cmbHour.Enabled = false;
            cmbMini.Enabled = false;
            txtStatus.Text = "运行中...";

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            is_start = false;

            config.autorun = 1;
            FPXml.SaveXml<Config>(config, FPFile.GetMapPath("/config.xml"));

            btnStart.Enabled = true;
            btnStop.Enabled = false;

            btnBackup.Enabled = true;
            btnSave.Enabled = true;
            cmbHour.Enabled = true;
            cmbMini.Enabled = true;
            txtStatus.Text = "已停止";
        }
    }
}
