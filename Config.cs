﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoZip
{
    public class Config
    {
        private string m_backup=String.Empty;
        private string m_saveas = String.Empty;
        private string m_hour = "00";
        private string m_minute = "00";
        private int m_autorun = 0;

        public string backup
        {
            get { return m_backup; }
            set { m_backup = value; }
        }

        public string saveas
        {
            get { return m_saveas; }
            set { m_saveas = value; }
        }

        public string hour
        {
            get { return m_hour; }
            set { m_hour = value; }
        }

        public string minute
        {
            get { return m_minute; }
            set { m_minute = value; }
        }

        public int autorun
        {
            get { return m_autorun; }
            set { m_autorun = value; }
        }

    }
}
